<?php
/**
 * Fichier html blog du template du député Philip Cordery
 * 
 * @author  Damien Senger
 * @version 1.0
 * 
 */

get_header(); ?>

    <div class="leftcol">
        <section class="social-links">
            <ul class="social-buttons"><!--
             --><li><a class="facebook" href="https://www.facebook.com/CorderyPhilip" title="Profil Facebook de Philip Cordery">Facebook</a></li><!--
             --><li><a class="twitter" href="https://twitter.com/philipcordery" title="Compte Twitter de Philip Cordery">Twitter</a></li><!--
             --><li><a class="videos" href="https://www.dailymotion.com/PhilipCordery" title="Découvrez les vidéos de Philip Cordery">Vidéos</a></li><!--
         --></ul>
        </section>
        
        <section class="content tag-cloud">
            <h4>Mots clés</h4>
            
            <?php 
                $tags = ilwp_tag_cloud(array(
                    'number' => 15,
                    'min_size' => 12,
                    'max_size' => 20,
                    'use_colors' => false,
                    'sort' => 'count'
                ));
                echo $tags;
            ?>
        </section>
        
        <?php 
            // On récupère la liste des 3 prochains événements
            $events = EM_Events::get(array('array' => true, 'limit' => 3, 'orderby' => 'event_start_date'));
            // On regarde le nombre d'événements trouvés
            if (count($events)) :
        ?>
        <section class="content next-events">
            <h4>Prochains événements</h4>
            
            <ul class="list-events">
                <?php 
                    // On récupère la liste des 3 prochains événements
                    $events = EM_Events::get(array('array' => 'true', 'limit' => 3, 'orderby' => 'event_start_date'));
                    
                    // On effectue du coup la boucle des prochains événements
                    foreach ($events as $event) :
                        // On récupère les informations sur le lieu
                        $lieu = EM_Locations::get(array('location' => $event['location_id'], 'limit' => 1));
                ?>
                <li class="event-<?php echo $event['event_id']; ?>">
                    <time datetime="<?php echo $event['event_start_date'] . ' ' . $event['event_start_time']; ?>">
                        <div class="calendar">
                            <span class="mois"><?php echo strftime('%b', strtotime($event['event_start_date'])); ?></span>
                            <span class="jour"><?php echo date('d', strtotime($event['event_start_date'])); ?></span>
                        </div>
                        <span class="heure"><?php echo date('H\hi', strtotime($event['event_start_date'] . ' ' . $event['event_start_time'])); ?></span>
                    </time>
                    <strong class="titre"><a href="<?php echo site_url('/events/' . $event['event_slug'] . '/'); ?>" class="nostyle"><?php echo $event['event_name']; ?></a></strong>
                    <p class="lieu"><?php echo $lieu[0]->location_name; ?><br><?php echo $lieu[0]->location_town; ?>&nbsp;(<?php echo $lieu[0]->location_country; ?>)</p>
                    <div class="clearfix"></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
        <?php endif; ?>
    </div><!--
    
 --><div class="maincol">
        <section class="content sous-cat">
            <a href="<?php echo site_url('/sujets/blog/terrain'); ?>" class="categorie">Mes articles sur le terrain</a>
            <a href="<?php echo site_url('/sujets/blog/positions'); ?>" class="categorie">Mes prises de position</a>
        </section>
     
        <section id="liste-articles">
            <?php 
                if (have_posts()) : while (have_posts()) : the_post(); get_template_part('content'); endwhile; endif; 
            ?>
            
            <div class="nav-previous alignleft"><?php next_posts_link( 'Articles précédents' ); ?></div>
            <div class="nav-next alignright"><?php previous_posts_link( 'Articles suivants' ); ?></div>
        </section>
    </div>
<?php
get_footer(); ?>
