        </main>

        <footer class="bottombar">
            <div class="footer-content">
                <section class="contact threecols">
                    <h5 id="contact">Contactez-moi</h5>
                    
                    <ul class="contacts">
                        <li>
                            <strong>Amandine Fouché (<em>Bruxelles</em>)</strong>
                            <a class="email" href="mailto:depute@philipcordery.fr">depute@philipcordery.fr</a>
                            <a class="tel" href="tel:+3222800114">+32&nbsp;2&nbsp;280&nbsp;01&nbsp;14</a>
                        </li>
                        <li>
                            <strong>Montaine Blonsard (<em>Paris</em>)</strong>
                            <a class="email" href="mailto:pcordery@assemblee-nationale.fr">pcordery@assemblee-nationale.fr</a>
                            <a class="tel" href="tel:+33140630658">+33&nbsp;1&nbsp;40&nbsp;63&nbsp;06&nbsp;58</a>
                        </li>
                        <li>
                            <strong>Laure Delcroix (<em>Presse</em>)</strong>
                            <a class="email" href="mailto:presse@philipcordery.fr">presse@philipcordery.fr</a>
                            <a class="tel" href="tel:+33786027729">+33&nbsp;7&nbsp;86&nbsp;02&nbsp;77&nbsp;29</a>
                        </li>
                    </ul>
                </section><!--
            
             --><section class="social threecols">
                    <h5>Réseaux sociaux</h5>
                    
                    <ul class="links-socialmedia">
                        <li>
                            <strong>Facebook :</strong>
                            <a class="network" href="https://facebook.com/CorderyPhilip" target="_blank">facebook.com/CorderyPhilip</a>
                        </li>
                        <li>
                            <strong>Twitter :</strong>
                            <a class="network" href="https://twitter.com/PhilipCordery" target="_blank">twitter.com/PhilipCordery</a>
                        </li>
                        <li>
                            <strong>Dailymotion :</strong>
                            <a class="network" href="https://dailymotion.com/PhilipCordery" target="_blank">dailymotion.com/PhilipCordery</a>
                        </li>
                    </ul>
                </section><!--
            
             --><section class="websites threecols">
                    <h5>Me Suivre</h5>
                    
                    <ul class="websites-links">
                        <li>
                            <strong>À l'Assemblée nationale :</strong>
                            <a href="http://www.assemblee-nationale.fr/14/tribun/fiches_id/610891.asp" target="_blank">assemblee-nationale.fr/14/tribun/fiches_id/610891.asp</a>
                        </li>
                        <li>
                            <strong>Au groupe socialiste, républicain et citoyen :</strong>
                            <a href="http://lessocialistes.fr" target="_blank">lessocialistes.fr</a>
                        </li>
                        <li>
                            <strong>Au Parti socialiste :</strong>
                            <a href="http://www.parti-socialiste.fr/intervenant/philip-cordery" target="_blank">parti-socialiste.fr/intervenant/philip-cordery</a>
                        </li>
                        <li>
                            <strong>Au Parti socialiste européen :</strong>
                            <a href="http://pes.eu/home_fr" target="_blank">pes.eu/home_fr</a>
                        </li>
                    </ul>
                </section>
            </div>
        </footer>
    </body>
</html>
