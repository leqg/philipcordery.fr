<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
        	<link rel="profile" href="http://gmpg.org/xfn/11">
        	<link rel="pingback" href="<?php echo bloginfo('pingback_url'); ?>">
        	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/logo-an.png">
        
        <!-- stylesheets & webfonts -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/print.css" media="print">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/icofont-pcordery.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/lightbox.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Merriweather+Sans:300,300italic,400,400italic,700,700italic,800,800italic">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Merriweather:300,300italic,400,400italic,700,700italic,900,900italic">
        
        <!-- scripts -->
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/lightbox.min.js"></script>
        
        <!-- wordpress generated head -->
        <?php wp_head(); ?>
    </head>
    <body <?php echo body_class(); ?>>
        <header class="topbar">
            <div class="topbar-content">
                <a href="<?php echo site_url(); ?>" class="nostyle"><h1 class="name">Philip Cordery</h1></a>
                <a href="#" class="toggleMenu">Menu</a>
                
                <?php 
                    $nav_args = array(
                        'theme_location' => 'header-menu',
                        'container' => 'nav',
                        'container_class' => 'main-nav',
                        'depth' => -1
                    );
                    wp_nav_menu($nav_args); 
                ?>
            </div>
            <div class="search-button" id="search-button">&#xe97d;</div>
            <?php get_search_form(); ?>
        </header>
        
        <?php if (is_front_page()) : ?>
            <div class="banner"></div>
        <?php endif; ?>
        
        <main>
