<?php
/**
 * Fichier principal du template du député Philip Cordery
 * 
 * @author  Damien Senger
 * @version 1.0
 * 
 */

get_header(); ?>

    <div class="leftcol">
        <?php get_template_part('social', 'links'); ?>
        
        <?php get_template_part('social', 'tweet'); ?>
        
        <?php 
            // On récupère la liste des 3 prochains événements
            $events = EM_Events::get(array('array' => true, 'limit' => 3, 'orderby' => 'event_start_date'));
            // On regarde le nombre d'événements trouvés
            if (count($events)) :
        ?>
        <section class="content next-events">
            <h4>Prochains événements</h4>
            
            <ul class="list-events">
                <?php 
                    // On récupère la liste des 3 prochains événements
                    $events = EM_Events::get(array('array' => 'true', 'limit' => 3, 'orderby' => 'event_start_date'));
                    
                    // On effectue du coup la boucle des prochains événements
                    foreach ($events as $event) :
                        // On récupère les informations sur le lieu
                        $lieu = EM_Locations::get(array('location' => $event['location_id'], 'limit' => 1));
                ?>
                <li class="event-<?php echo $event['event_id']; ?>">
                    <time datetime="<?php echo $event['event_start_date'] . ' ' . $event['event_start_time']; ?>">
                        <div class="calendar">
                            <span class="mois"><?php echo strftime('%b', strtotime($event['event_start_date'])); ?></span>
                            <span class="jour"><?php echo date('d', strtotime($event['event_start_date'])); ?></span>
                        </div>
                        <span class="heure"><?php echo date('H\hi', strtotime($event['event_start_date'] . ' ' . $event['event_start_time'])); ?></span>
                    </time>
                    <strong class="titre"><a href="<?php echo site_url('/events/' . $event['event_slug'] . '/'); ?>" class="nostyle"><?php echo $event['event_name']; ?></a></strong>
                    <p class="lieu"><?php echo $lieu[0]->location_name; ?><br><?php echo $lieu[0]->location_town; ?>&nbsp;(<?php echo $lieu[0]->location_country; ?>)</p>
                    <div class="clearfix"></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
        <?php endif; ?>
        
        <section class="content videos">
            <h4>Dernières vidéos</h4>
            <object class="dm_videowall logo_left" 
                    data="http://www.dailymotion.com/videowall/user/PhilipCordery&amp;cols=3&amp;space=5&amp;slide=0&amp;zoom=0&amp;auto=0&amp;shadow=0" 
                    type="application/x-shockwave-flash" 
                    height="200" 
                    width="280">
                <param name="movie" value="http://www.dailymotion.com/videowall/user/PhilipCordery&amp;cols=3&amp;space=5&amp;slide=0&amp;zoom=0&amp;auto=0&amp;shadow=0">
                <param name="allowscriptaccess" value="always">
                <param name="wmode" value="transparent">
            </object>
        </section>
        
        <?php get_template_part('aside', 'photos'); ?>

    </div><!--
    
 --><div class="maincol">
        <section class="newsletter-bar">
            <form action="http://cordery.leqg.info/mail-ajout.php" method="post">
                <input class="newsletter-email" type="email" name="email" placeholder="Indiquer votre adresse mail pour suivre mon actualité">
                <input class="newsletter-valid" type="submit" value="M'inscrire à la lettre d'information">
            </form>
        </section>
        
        <section id="liste-articles">
            <?php 
                query_posts('cat=61'); if (have_posts()) : while (have_posts()) : the_post(); get_template_part('content'); endwhile; endif; 
            ?>
            
            <a href="<?php echo site_url('/blog/'); ?>" class="goBlog">Charger les articles suivants</a>
        </section>
    </div>
<?php
get_footer(); ?>
