<section class="social-media-access">
    <ul><!--
     --><a href="https://www.facebook.com/CorderyPhilip" title="Profil Facebook de Philip Cordery" target="_blank"><li class="facebook">Facebook</li></a><!--
     --><a href="https://twitter.com/philipcordery" title="Compte Twitter de Philip Cordery" target="_blank"><li class="twitter">Twitter</li></a><!--
     --><a href="https://www.dailymotion.com/PhilipCordery" title="Découvrez les vidéos de Philip Cordery" target="_blank"><li class="videos">Vidéos</li></a><!--
 --></ul>
</section>
