<?php

    // On tente de forcer une locale
    setlocale(LC_TIME, 'fr_FR.UTF-8');

    // Préparation à l'enregistrement des menus
    function register_menus()
    {
        register_nav_menus(array(
            'header-menu' => __('Header Menu'),
            'logos-footer' => __('Logos in footer')
        ));
    }

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
    
    // Enregistrement des menus
    add_action('init', 'register_menus');
    
    // Activation des miniatures d'article
    if (function_exists('add_theme_support')) {
      add_theme_support('post-thumbnails');
    }
    