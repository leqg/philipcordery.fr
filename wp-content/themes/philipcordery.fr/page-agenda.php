<?php
/**
 * Fichier html blog du template du député Philip Cordery
 * 
 * @author  Damien Senger
 * @version 1.0
 * 
 */

get_header(); ?>

    <div class="leftcol">
        <section class="content">
            <h4>Calendrier des événements</h4>
            
            <?php
                $calendar = EM_Calendar::output();
                echo $calendar;
            ?>
        </section>
    </div><!--
    
 --><div class="maincol">
        <h2 class="next-events">Événements à venir</h2>
 
        <section class="next-dates">
            <ul class="list-events">
                <?php 
                    // On récupère la liste des 3 prochains événements
                    $events = EM_Events::get(array('array' => 'true', 'limit' => 15, 'orderby' => 'event_start_date'));
                    
                    // On effectue du coup la boucle des prochains événements
                    foreach ($events as $event) :
                        // On récupère les informations sur le lieu
                        $lieu = EM_Locations::get(array('location' => $event['location_id'], 'limit' => 1));
                ?>
                <li class="event-<?php echo $event['event_id']; ?>">
                    <time datetime="<?php echo $event['event_start_date'] . ' ' . $event['event_start_time']; ?>">
                        <div class="calendar">
                            <span class="mois"><?php echo strftime('%b', strtotime($event['event_start_date'])); ?></span>
                            <span class="jour"><?php echo date('d', strtotime($event['event_start_date'])); ?></span>
                        </div>
                        <span class="heure"><?php echo date('H\hi', strtotime($event['event_start_date'] . ' ' . $event['event_start_time'])); ?></span>
                    </time>
                    <strong class="titre"><a href="<?php echo site_url('/events/' . $event['event_slug'] . '/'); ?>" class="nostyle"><?php echo $event['event_name']; ?></a></strong>
                    <p class="lieu"><?php echo $lieu[0]->location_name; ?><br><?php echo $lieu[0]->location_town; ?>&nbsp;(<?php echo $lieu[0]->location_country; ?>)</p>
                    <?php if (!is_null($event['post_content'])) : ?><p class="desc"><?php echo $event['post_content']; ?></p><?php endif; ?>
                    <div class="clearfix"></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </div>
<?php
get_footer(); ?>
