<section class="content tag-cloud">
    <h4>Mots clés</h4>
    
    <?php 
    if (function_exists('wp_tag_cloud')) {
        $args = array(
            'smallest'  =>  12,
            'largest'   =>  15,
            'unit'      =>  'pt',
            'number'    =>  12,
            'format'    =>  'list',
            'orderby'   =>  'count',
            'order'     =>  'DESC',
            'link'      =>  'view',
            'taxonomy'  =>  'post_tag',
            'echo'      =>  'true'
        );
        
        wp_tag_cloud($args);
    }
    ?>
</section>
