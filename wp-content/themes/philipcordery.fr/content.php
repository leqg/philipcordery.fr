<article class="content extract-article article-<?php the_ID(); ?>">
    <a class="image-miniature" href="<?php the_permalink(); ?>"><?php if (has_post_thumbnail()) the_post_thumbnail(); ?></a>
    <h3><a href="<?php the_permalink(); ?>" class="inverse"><?php the_title(); ?></a></h3>
    <?php the_excerpt(); ?>
    <p class="infos">Article mis en ligne le <?php the_date(); ?>.</p>
    <div class="clearfix"></div>
</article>
