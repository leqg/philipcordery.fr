<section class="content social-media-access social-sharing">
    <h4>Partagez cet article</h4>
    
    <ul><!--
     --><a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Cliquez pour partager ce lien sur Facebook" target="_blank"><li class="facebook">Facebook</li></a><!--
     --><a href="https://twitter.com/home?status=+via+@PhilipCordery+<?php the_permalink(); ?>" title="Cliquez pour tweeter cet article" target="_blank"><li class="twitter">Twitter</li></a><!--
     --><a href="javascript:window.print()" title="Imprimez cet article de Philip Cordery"><li class="print">Vidéos</li></a><!--
 --></ul>
</section>
