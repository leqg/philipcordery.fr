<article class="content content-article article article-<?php the_ID(); ?>">
    <h3><?php the_title(); ?></h3>
    <?php $cats = get_the_category(); ?>
    <p class="infos">Article mis en ligne le <?php the_date(); if (count($cats)) : ?> dans <?php the_category(','); endif; ?>.</p>
    <?php if (has_post_thumbnail()) the_post_thumbnail(); ?>
    <?php the_content(); ?>
    <div class="clearfix"></div>
</article>
