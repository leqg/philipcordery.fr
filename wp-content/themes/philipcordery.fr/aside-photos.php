    <section class="content photos">
        <h4><a href="https://plus.google.com/photos/101729562480476226142" class="nostyle" target="_blank">Galerie photo</a></h4>
        
        <ul class="pictures-widget"><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc1.jpg" data-lightbox="picgal"><img class="portrait" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc1.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc2.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc2.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc3.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc3.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc4.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc4.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc5.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc5.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc6.jpg" data-lightbox="picgal"><img class="portrait" src="<?php echo get_template_directory_uri(); ?>/assets/img/pc6.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc7.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc7.jpg"></a></li><!--
            --><li><a href="<?php echo get_template_directory_uri(); ?>/assets/img/pc8.jpg" data-lightbox="picgal"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc8.jpg"></a></li><!--
        --></ul>
    </section>
