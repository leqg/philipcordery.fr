# MP Philip Cordery's website
Wordpress' theme for MP Philip Cordery's website

## Installation du thème WordPress
### Plugins nécessaires
- [Events Manager](https://wordpress.org/plugins/events-manager/)
- [Jetpack by WordPress.com](https://wordpress.org/plugins/jetpack/)
- [WordPress SEO by Yoast](https://wordpress.org/plugins/wordpress-seo/)
